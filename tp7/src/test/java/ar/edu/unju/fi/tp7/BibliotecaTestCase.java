package ar.edu.unju.fi.tp7;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ar.edu.unju.fi.tp7.entity.Libro;
import ar.edu.unju.fi.tp7.entity.Miembro;
import ar.edu.unju.fi.tp7.service.LibroService;
import ar.edu.unju.fi.tp7.service.MiembroService;

@SpringBootTest
class BibliotecaTestCase{

	@Autowired
	LibroService libroService;
	
	@Autowired
	MiembroService miembroService;
	
	static Libro libroNuevo;
	static Libro libroNuevo2;
	static Libro libroNuevo3;
	static Miembro miembroNuevo;
	static Miembro miembroNuevo2;
	static Miembro miembroNuevo3;
	
	@BeforeEach
	void setUp() throws Exception{
		libroNuevo = libroService.crearLibro();
		libroNuevo.setIsbn(677);
		libroNuevo.setAutor("milton");
		libroNuevo2 = libroService.crearLibro();
		libroNuevo2.setAutor("delgado");
		libroNuevo3 = libroService.crearLibro();
		libroNuevo3.setTitulo("matematicas");
		miembroNuevo = miembroService.crearMiembro();
		miembroNuevo.setCodigo(100);
		miembroNuevo2 = miembroService.crearMiembro();
		miembroNuevo.setCodigo(200);
		
	}
	
	@AfterEach
	void tearDown() throws Exception{
		libroService.eliminarLibro(libroNuevo);
		libroService.eliminarLibro(libroNuevo2);
		libroService.eliminarLibro(libroNuevo3);
		miembroService.eliminarMiembro(miembroNuevo);
		miembroService.eliminarMiembro(miembroNuevo2);
	}
	/**
	 * Prueba la funcionalidad de guardar y crear un libro.
	 * Se verifica que un libro se guarda correctamente y que se puede encontrar por su ISBN.
	 */
	@Test
	@DisplayName("Guardar y crear libro")
	void GuardarCrearLibroTest(){
		Integer cantidad = libroService.getAll().size();
		libroService.guardarLibro(libroNuevo);
		assertNotEquals(null,libroService.findByIsbn(677));
		assertTrue(libroService.getAll().size() > cantidad);
	}
	/**
	 * Prueba la funcionalidad de eliminar un libro.
	 * Se verifica que un libro se elimina correctamente y que ya no se puede encontrar por el autor.
	 */
	@Test
	@DisplayName("eliminar libro")
	void elimianLibroTest(){
		libroService.guardarLibro(libroNuevo);
		Integer cantidad = libroService.getAll().size();
		libroService.eliminarLibro(libroNuevo);
		assertNull(libroService.findByAutor("milton"));
		assertTrue(cantidad > libroService.getAll().size());
	}
	/**
	 * Prueba la funcionalidad de editar un libro.
	 * Se verifica que un libro se puede editar correctamente y que los cambios se reflejan en la base de datos.
	 */
	@Test
	@DisplayName("editar libro")
	void editarLibroTest(){
		libroService.guardarLibro(libroNuevo);
		int cantidad = libroService.getAll().size();
		assertNotNull(libroService.findByAutor("milton"));
		libroNuevo2.setId(libroNuevo.getId());
		libroService.modificarLibro(libroNuevo2);
		assertEquals(cantidad,libroService.getAll().size());
		assertNotNull(libroService.findByAutor("delgado"));
		assertNull(libroService.findByAutor("milton"));
	}
	/**
	 * Prueba la funcionalidad de buscar un libro por diferentes criterios.
	 * Se verifica que se pueden buscar libros por ISBN, título y autor.
	 */
	@Test
	@DisplayName("Buscar libro")
	void buscarLibroTest(){
		libroService.guardarLibro(libroNuevo);
		assertNotNull(libroService.findByIsbn(677));
		assertNull(libroService.findByIsbn(233));
		libroService.guardarLibro(libroNuevo3);
		assertNotNull(libroService.findByTitulo("matematicas"));
		assertNull(libroService.findByTitulo("medicina"));
		assertNotNull(libroService.findByAutor("milton"));
		assertNull(libroService.findByAutor("mateo"));
		libroNuevo2.setId(libroNuevo.getId());
		libroService.modificarLibro(libroNuevo2);
		assertNull(libroService.findByAutor("milton"));
	}
	
	/**
	 * Prueba la funcionalidad de guardar y crear un miembro.
	 * Se verifica que un miembro se guarda correctamente y que se puede encontrar por su código.
	 */
	@Test
	@DisplayName("Guardar y crear miembro")
	void GuardarCrearMiembroTest(){
		Integer cantidad = miembroService.getAll().size();
		miembroService.guardarMiembro(miembroNuevo);
		Miembro miembroEncontrado = miembroService.findByCodigo((miembroNuevo.getCodigo())); 
		assertNotNull(miembroEncontrado);
		assertTrue(miembroService.getAll().size() > cantidad);
	}
	/**
	 * Prueba la funcionalidad de eliminar un miembro.
	 * Se verifica que un miembro se elimina correctamente y que ya no se puede encontrar por su código.
	 */
	@Test
	@DisplayName("eliminar miembro")
	void eliminarLibroMiembroTest(){
		miembroService.guardarMiembro(miembroNuevo);
		Integer cantidad = miembroService.getAll().size();
		miembroService.eliminarMiembro(miembroNuevo);
		assertNull(miembroService.findByCodigo(100));
		assertTrue(cantidad > miembroService.getAll().size());
	}
	/**
	 * Prueba la funcionalidad de editar un miembro.
	 * Se verifica que un miembro se puede editar correctamente y que los cambios se reflejan en la base de datos.
	 */
	@Test
	@DisplayName("editar miembro")
	void editarLibroMiembroTest(){
		miembroService.guardarMiembro(miembroNuevo);
		int cantidad = miembroService.getAll().size();
		assertNotNull(miembroService.findByCodigo(miembroNuevo.getCodigo()));
		miembroNuevo2.setId(miembroNuevo.getId());
		miembroService.editarMiembro(miembroNuevo2);
		assertEquals(cantidad,miembroService.getAll().size());
		assertNotNull(miembroService.findByCodigo((miembroNuevo2.getCodigo())));
		assertNull(miembroService.findByCodigo((miembroNuevo.getCodigo())));
	}
}

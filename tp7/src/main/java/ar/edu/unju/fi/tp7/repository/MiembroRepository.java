package ar.edu.unju.fi.tp7.repository;

import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


import ar.edu.unju.fi.tp7.entity.Miembro;

@Repository
public interface MiembroRepository extends CrudRepository<Miembro,Long>{
	 /**
     * Obtiene una lista de todos los miembros en la base de datos.
     *
     * @return Una lista de miembros.
     */
	 public ArrayList<Miembro> findAll();
	     /**
	     * Busca un miembro por su código.
	     *
	     * @param codigo El código del miembro a buscar.
	     * @return El miembro encontrado o null si no se encuentra.
	     */
	 public Miembro findByCodigo(Integer codigo);
}

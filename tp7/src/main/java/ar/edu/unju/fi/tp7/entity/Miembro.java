package ar.edu.unju.fi.tp7.entity;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "Miembro")
public class Miembro {
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "miembro_id")
   private Long id;
   @Column(name="Nombre")
   private String nombre;
   @Column(name="Codigo")
   private Integer codigo;
   @Column(name="Correo_Electronico")
   private String correoElectronico;
   @Column(name="Telefono")
   private Integer telefono;
   @OneToMany(mappedBy= "miembro")
   private List<Libro> libros;
   
   public Miembro() {
	   
   }


public Long getId() {
	return id;
}


public void setId(Long id) {
	this.id = id;
}


public String getNombre() {
	return nombre;
}


public void setNombre(String nombre) {
	this.nombre = nombre;
}


public Integer getCodigo() {
	return codigo;
}


public void setCodigo(Integer codigo) {
	this.codigo = codigo;
}


public String getCorreoElectronico() {
	return correoElectronico;
}


public void setCorreoElectronico(String correoElectronico) {
	this.correoElectronico = correoElectronico;
}


public Integer getTelefono() {
	return telefono;
}


public void setTelefono(Integer telefono) {
	this.telefono = telefono;
}

public List<Libro> getLibros(){
  return libros;	
}
public void setLibros(List<Libro> libros){
	this.libros = libros;
}

@Override
public String toString() {
	return "Miembro [id=" + id + ", nombre=" + nombre + ", codigo=" + codigo + ", correoElectronico="
			+ correoElectronico + ", telefono=" + telefono + ", libros=" + libros + "]";
}



}


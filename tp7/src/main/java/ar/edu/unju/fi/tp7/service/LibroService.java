package ar.edu.unju.fi.tp7.service;

import java.util.ArrayList;

import ar.edu.unju.fi.tp7.entity.Libro;

public interface LibroService {
	/**
     * Guarda un libro en la biblioteca.
     *
     * @param libro El libro a ser guardado.
     */
	public void guardarLibro(Libro libro);
	/**
     * Modifica un libro existente en la biblioteca.
     *
     * @param libro El libro a ser modificado.
     */
	public void modificarLibro(Libro libro);
	 /**
     * Elimina un libro de la biblioteca.
     *
     * @param libro El libro a ser eliminado.
     */
	public void eliminarLibro(Libro libro);
	/**
     * Crea y devuelve un nuevo objeto Libro.
     *
     * @return Un nuevo objeto Libro.
     */
	public Libro crearLibro();
	/**
     * Obtiene una lista de todos los libros en la biblioteca.
     *
     * @return Una lista de libros.
     */
	public ArrayList<Libro> getAll();
	/**
     * Busca un libro por su título en la biblioteca.
     *
     * @param titulo El título del libro a buscar.
     * @return El libro encontrado o null si no se encuentra.
     */
	public Libro findByTitulo(String titulo);
	/**
     * Busca un libro por su ISBN en la biblioteca.
     *
     * @param isbn El ISBN del libro a buscar.
     * @return El libro encontrado o null si no se encuentra.
     */
	public Libro findByIsbn(Integer isbn);
	/**
     * Busca un libro por su autor en la biblioteca.
     *
     * @param autor El autor del libro a buscar.
     * @return El libro encontrado o null si no se encuentra.
     */
	public Libro findByAutor(String autor);
}

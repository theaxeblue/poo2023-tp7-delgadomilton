package ar.edu.unju.fi.tp7.entity;


import java.util.ArrayList;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "Libro")
public class Libro {
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "libro_id")
   private Long id;
   @Column(name="Titulo")
   private String titulo;
   @Column(name="Autor")
   private String autor;
   @Column(name="ISBN")
   private Integer isbn;
   @Column(name="Cantidad_Disponible")
   private Integer cantidadDisponible;
   @ManyToOne
   @JoinColumn(name = "miembro_id", nullable = true)
   private Miembro miembro;
   public Libro(){
	   
   }
   
public Long getId() {
	return id;
}
public void setId(Long id) {
	this.id = id;
}
public String getTitulo() {
	return titulo;
}
public void setTitulo(String titulo) {
	this.titulo = titulo;
}
public String getAutor() {
	return autor;
}
public void setAutor(String autor) {
	this.autor = autor;
}
public Integer getIsbn() {
	return isbn;
}
public void setIsbn(Integer isbn) {
	this.isbn = isbn;
}
public Integer getCantidadDisponible() {
	return cantidadDisponible;
}
public void setCantidadDisponible(Integer cantidadDisponible) {
	this.cantidadDisponible = cantidadDisponible;
}

public Miembro getMiembro() {
	return miembro;
}
public void setMiembro(Miembro miembro){
	this.miembro = miembro;
}

@Override
public String toString() {
	return "Libro [id=" + id + ", titulo=" + titulo + ", autor=" + autor + ", isbn=" + isbn + ", cantidadDisponible="
			+ cantidadDisponible + "]";
}
   
}

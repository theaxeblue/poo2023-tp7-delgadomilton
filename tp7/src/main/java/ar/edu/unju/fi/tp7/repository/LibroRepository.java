package ar.edu.unju.fi.tp7.repository;

import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ar.edu.unju.fi.tp7.entity.Libro;

@Repository
public interface LibroRepository extends CrudRepository<Libro,Long>{

	/**
     * Busca un libro por su título.
     *
     * @param titulo El título del libro a buscar.
     * @return El libro encontrado o null si no se encuentra.
     */
	public Libro findByTitulo(String titulo);
	/**
     * Busca un libro por su ISBN.
     *
     * @param isbn El ISBN del libro a buscar.
     * @return El libro encontrado o null si no se encuentra.
     */
	public Libro findByIsbn(Integer isbn);
	/**
     * Busca un libro por su autor.
     *
     * @param autor El autor del libro a buscar.
     * @return El libro encontrado o null si no se encuentra.
     */
	public Libro findByAutor(String autor);
	/**
     * Obtiene una lista de todos los libros en la biblioteca.
     *
     * @return Una lista de libros.
     */
	public ArrayList<Libro> findAll();
}

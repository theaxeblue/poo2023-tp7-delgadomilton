package ar.edu.unju.fi.tp7.service.imp;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.unju.fi.tp7.entity.Miembro;
import ar.edu.unju.fi.tp7.repository.MiembroRepository;
import ar.edu.unju.fi.tp7.service.MiembroService;

@Service
public class MiembroServiceImp implements MiembroService {

	@Autowired
	private MiembroRepository miembroRepository;
	
	/**
	 * Guarda un miembro en la base de datos.
	 *
	 * @param miembro El miembro a ser guardado.
	 */
	@Override
	public void guardarMiembro(Miembro miembro) {
		// TODO Auto-generated method stub
		miembroRepository.save(miembro);
	}
	/**
	 * Edita un miembro existente en la base de datos.
	 *
	 * @param miembro El miembro a ser editado.
	 */
	@Override
	public void editarMiembro(Miembro miembro) {
		// TODO Auto-generated method stub
		miembroRepository.save(miembro);
	}
	/**
	 * Elimina un miembro de la base de datos.
	 *
	 * @param miembro El miembro a ser eliminado.
	 */
	@Override
	public void eliminarMiembro(Miembro miembro) {
		// TODO Auto-generated method stub
		miembroRepository.delete(miembro);
	}
	/**
	 * Crea y devuelve un nuevo objeto Miembro.
	 *
	 * @return Un nuevo objeto Miembro.
	 */
	@Override
	public Miembro crearMiembro() {
		// TODO Auto-generated method stub
		return new Miembro();
	}
	/**
	 * Obtiene una lista de todos los miembros en la base de datos.
	 *
	 * @return Una lista de miembros.
	 */
	@Override
	public ArrayList<Miembro> getAll() {
		// TODO Auto-generated method stub
		return miembroRepository.findAll();
	}
	/**
	 * Busca un miembro por su código en la base de datos.
	 *
	 * @param codigo El código del miembro a buscar.
	 * @return El miembro encontrado o null si no se encuentra.
	 */
	@Override
	public Miembro findByCodigo(Integer codigo) {
		// TODO Auto-generated method stub
		return miembroRepository.findByCodigo(codigo);
	}

}
package ar.edu.unju.fi.tp7.service;

import java.util.ArrayList;

import ar.edu.unju.fi.tp7.entity.Miembro;

public interface MiembroService {

	 /**
     * Guarda un miembro en la biblioteca.
     *
     * @param miembro El miembro a ser guardado.
     */
	public void guardarMiembro(Miembro miembro);
	/**
     * Edita un miembro existente en la biblioteca.
     *
     * @param miembro El miembro a ser editado.
     */
	public void editarMiembro(Miembro miembro);
	/**
     * Elimina un miembro de la biblioteca.
     *
     * @param miembro El miembro a ser eliminado.
     */
	public void eliminarMiembro(Miembro miembro);
	/**
     * Crea y devuelve un nuevo objeto Miembro.
     *
     * @return Un nuevo objeto Miembro.
     */
	public Miembro crearMiembro();
	/**
     * Obtiene una lista de todos los miembros en la biblioteca.
     *
     * @return Una lista de miembros.
     */
	public ArrayList<Miembro> getAll();
	/**
     * Busca un miembro por su código en la biblioteca.
     *
     * @param codigo El código del miembro a buscar.
     * @return El miembro encontrado o null si no se encuentra.
     */
	public Miembro findByCodigo(Integer codigo);
	
}

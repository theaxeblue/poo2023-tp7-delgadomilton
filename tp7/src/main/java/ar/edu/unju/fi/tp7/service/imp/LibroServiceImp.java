package ar.edu.unju.fi.tp7.service.imp;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.unju.fi.tp7.entity.Libro;
import ar.edu.unju.fi.tp7.repository.LibroRepository;
import ar.edu.unju.fi.tp7.service.LibroService;

@Service
public class LibroServiceImp implements LibroService {

	@Autowired
	private LibroRepository libroRepository;

	/**
	 * Guarda un libro en la base de datos.
	 *
	 * @param libro El libro a ser guardado.
	 */
	@Override
	public void guardarLibro(Libro libro) {
		// TODO Auto-generated method stub
		libroRepository.save(libro);
	}
	/**
	 * Modifica un libro existente en la base de datos.
	 *
	 * @param libro El libro a ser modificado.
	 */
	@Override
	public void modificarLibro(Libro libro) {
		// TODO Auto-generated method stub
		libroRepository.save(libro);
	}
	/**
	 * Elimina un libro de la base de datos.
	 *
	 * @param libro El libro a ser eliminado.
	 */
	@Override
	public void eliminarLibro(Libro libro) {
		// TODO Auto-generated method stub
		libroRepository.delete(libro);
	}
	/**
	 * Crea y devuelve un nuevo objeto Libro.
	 *
	 * @return Un nuevo objeto Libro.
	 */
	@Override
	public Libro crearLibro() {
		// TODO Auto-generated method stub
		return new Libro();
	}
	/**
	 * Busca un libro por su título en la base de datos.
	 *
	 * @param titulo El título del libro a buscar.
	 * @return El libro encontrado o null si no se encuentra.
	 */
	@Override
	public Libro findByTitulo(String titulo) {
		// TODO Auto-generated method stub
		return libroRepository.findByTitulo(titulo);
	}
	/**
	 * Busca un libro por su ISBN en la base de datos.
	 *
	 * @param isbn El ISBN del libro a buscar.
	 * @return El libro encontrado o null si no se encuentra.
	 */
	@Override
	public Libro findByIsbn(Integer isbn){
		// TODO Auto-generated method stub
		return libroRepository.findByIsbn(isbn);
	}
	/**
	 * Busca un libro por su autor en la base de datos.
	 *
	 * @param autor El autor del libro a buscar.
	 * @return El libro encontrado o null si no se encuentra.
	 */
	@Override
	public Libro findByAutor(String titulo){
		// TODO Auto-generated method stub
		return libroRepository.findByAutor(titulo);
	}
	/**
	 * Obtiene una lista de todos los libros en la base de datos.
	 *
	 * @return Una lista de libros.
	 */
	@Override
	public ArrayList<Libro> getAll() {
		// TODO Auto-generated method stub
		return libroRepository.findAll();
	}

}
